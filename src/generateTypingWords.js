import { available_words } from "./available_words.js";

let words = [];
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
function get_checked_characters(settingsData) {
  let chars = settingsData.chars
    .filter((t) => {
      return t.checked == true;
    })
    .map((t) => {
      return t.char;
    });
  let accents = settingsData.accents
    .filter((t) => {
      return t.checked == true;
    })
    .map((t) => {
      return t.char;
    });
  return chars + accents;
}
function get_required_words(list_chars, words) {
  let bag_word = [];
  words.forEach(function (word) {
    let skip = false;
    word.split("").forEach(function (character) {
      if (!list_chars.includes(character)) {
        skip = true;
        return;
      }
    });

    if (skip == false) {
      bag_word.push(word);
    } //keep only words that contains characters selected
  });
  return bag_word;
}

function get_random_words(list_of_words, how_many) {
  let result = [];
  while (result.length < how_many) {
    let el = getRandomInt(0, list_of_words.length);
    let word = list_of_words[el];
    list_of_words.splice(el, 1);
    result.push(word);
  }
  return result;
}
export function generate_text(settingsData, failed_words = false) {
  let words = available_words[settingsData.wordsLanguage];
  let chars = get_checked_characters(settingsData).split("");
  let all_words = get_required_words(chars, words);
  let some_words = [];
  let words_to_type = [];

  console.log("a w ", all_words);
  if (all_words.length > 0) {
    some_words = get_random_words(all_words, settingsData.wordsCount);
  }
  if (failed_words != false) {
    some_words = failed_words;
  }

  some_words.forEach(function (word) {
    word.split("").forEach(function (character) {
      let character_object = {
        char: character,
        error: false,
        active: false,
        typed: false,
      };
      words_to_type.push(character_object);
    });

    let character_object = {
      char: " ",
      error: false,
      active: false,
      typed: false,
    };
    words_to_type.push(character_object);
  });
  return words_to_type;
}
