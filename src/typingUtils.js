export function groupCharsToWords(chars) {
  console.log("grouping chars");
  let grouped_chars = [];
  let word = [];
  chars.forEach(function (ch) {
    word = [...word, ch];
    if (ch.char == " ") {
      grouped_chars = [...grouped_chars, word];
      word = [];
    }
  });
  return grouped_chars;
}

export function splitCharsToLetters(chars) {
  console.log("splitting  chars");
  let space = { char: " ", error: false, active: false };
  let grouped_chars = [];
  chars.forEach(function (ch) {
    grouped_chars = [...grouped_chars, ch, space];
  });
  return [grouped_chars];
}
function createWord(d) {
  //console.log(d)
  let word = "";
  d.forEach(function (key) {
    if (key.char != " ") {
      word += key.char;
    }
  });
  return word;
}
export function getWord(grouped_chars, current_index) {
  let index = 0;
  for (const word of grouped_chars) {
    for (const ch of word) {
      if (current_index == index) {
        return createWord(word);
      }

      index++;
    }
  }
}

export function calculateCPM(index, end, start) {
  let cpm;
  cpm = Math.round((index / ((end - start) / 1000)) * 60);
  if (cpm != Infinity && index > 5) {
    // start showing CPM only after few characters
    return cpm;
  }
}

export function logFailedKey(failingChars, key) {
  if (key == " ") {
    return;
  }
  if (failingChars.hasOwnProperty(key)) {
    failingChars[key] += 1;
  } else {
    failingChars[key] = 1;
  }

  failingChars = Object.entries(failingChars)
    .sort(([, a], [, b]) => b - a)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
  return failingChars;
}
