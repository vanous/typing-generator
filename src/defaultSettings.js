import { defaultSettings_cz } from "./defaultSettings_cz.js";
import { defaultSettings_en } from "./defaultSettings_en.js";

export let defaultSettings = {};

let global_settings = {
  wordsCount: 30,
  instantDeath: false,
  boardSize: 7,
  showErrors: true,
  showCursor: true,
  showStats: false,
  showCharacters: true,
  showDebug: false,
  tts: {
    pitch: 1,
    rate: 0.8,
    voice: 0,
    volume: 1.5,
  },
};
defaultSettings["cz"] = { ...defaultSettings_cz, ...global_settings };
defaultSettings["en"] = { ...defaultSettings_en, ...global_settings };
