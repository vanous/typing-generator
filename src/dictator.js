export function speak(text, settingsData) {
  let synth = window.speechSynthesis;
  console.log("doing something", text);
  if (synth.speaking) {
    console.error("speechSynthesis.speaking");
    return;
  }
  if (text !== "") {
    var utterThis = new SpeechSynthesisUtterance(text);
    utterThis.onend = function (event) {
      console.log("SpeechSynthesisUtterance.onend");
    };
    utterThis.onerror = function (event) {
      console.error("SpeechSynthesisUtterance.onerror");
    };
    utterThis.lang = settingsData.languageCode;
    utterThis.pitch = settingsData.tts.pitch;
    utterThis.rate = settingsData.tts.rate;
    synth.speak(utterThis);
  }
}
