export let typingStateDefault = {
  failedWords: [],
  failingChars: {},
  current_index: 0,
  error_count: 0,
  trials: 1,
  finished: false,
  cpm: null,
  awaitKey: false,
  awaitKeyValue: "",
  awaitKeyConfirmed: false,
  startDictating: false,
};
