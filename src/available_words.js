import { words_cz } from "./words_cz.js";
import { words_en } from "./words_en.js";

export let available_words = {};

available_words["cz"] = words_cz;
available_words["en"] = words_en;
