- New version: https://vanous.codeberg.page/typing-tutor
- Original version: here: https://vanous.codeberg.page/typing-generator

Original author of inspirational code: [paulfab / touchtyping](https://github.com/paulfab/touchtyping.git)

Word list source: <a href="https://korpus.cz/">Český národní korpus</a>, <a href="https://wiki.korpus.cz/doku.php/cnk:syn2015">Korpus SYN2015</a>, linked on <a href="https://wiki.korpus.cz/doku.php/seznamy:srovnavaci_seznamy">Srovnávací seznamy</a> in <a href="https://wiki.korpus.cz/doku.php/seznamy:srovnavaci_seznamy#download">Download</a>, direct link to <a href="https://wiki.korpus.cz/lib/exe/fetch.php/seznamy:syn2015_lemma_utf8.zip">Lemmata SYN2015 syn2015_lemma_utf8-2.zip 1.2MB</a>.
